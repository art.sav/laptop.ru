<?php

namespace App\Calculators;

use \App\Interfaces\IComplexAlgebraic;
use \App\models\ComplexAlgebraic;

/*
    Calculator for Complex Number in Algebraic Form
*/
class ComplexCalculatorAlgebraic
{

    static public function addition(IComplexAlgebraic $complex1, IComplexAlgebraic $complex2)
    {
        return new ComplexAlgebraic(
                    $complex1->getReal() + $complex2->getReal(),
                    $complex1->getImaginary() + $complex2->getImaginary()
                );
    }

    static public function subtraction(IComplexAlgebraic $complex1, IComplexAlgebraic $complex2)
    {
        return new ComplexAlgebraic(
                    $complex1->getReal() - $complex2->getReal(),
                    $complex1->getImaginary() - $complex2->getImaginary()
                );
    }

    static public function multiplication(IComplexAlgebraic $complex1, IComplexAlgebraic $complex2)
    {
        $real = ($complex1->getReal() * $complex2->getReal()) - ($complex1->getImaginary() * $complex2->getImaginary());
        $imaginary = ($complex1->getReal() * $complex2->getImaginary()) + ($complex1->getImaginary() * $complex2->getReal());
        return new ComplexAlgebraic(
                    $real,
                    $imaginary
                );
    }

    static public function division(IComplexAlgebraic $complex1, IComplexAlgebraic $complex2)
    {
        $conjugateComplex2 = $complex2->getConjugate();

        $numerator = self::multiplication($complex1, $conjugateComplex2);
        $denominator = self::multiplication($complex2, $conjugateComplex2);

        return new ComplexAlgebraic(
                    $numerator->getReal() / $denominator->getReal(),
                    $numerator->getImaginary() / $denominator->getReal()
                );
    }

}
