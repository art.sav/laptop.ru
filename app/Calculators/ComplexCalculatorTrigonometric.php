<?php

namespace App\Calculators;

use \App\Interfaces\IComplexTrigonometric;
use \App\models\ComplexTrigonometric;
use \App\Exceptions\NeedConvertException;

/*
    Calculator for Complex Number in Algebraic Form
*/
class ComplexCalculatorTrigonometric
{

    static public function addition(IComplexTrigonometric $complex1, IComplexTrigonometric $complex2)
    {
        throw new NeedConvertException("Need convert to Algebraic form", 1);
    }

    static public function subtraction(IComplexTrigonometric $complex1, IComplexTrigonometric $complex2)
    {
        throw new NeedConvertException("Need convert to Algebraic form", 1);
    }

    // without calculate
    static public function multiplication(IComplexTrigonometric $complex1, IComplexTrigonometric $complex2)
    {
        return new ComplexTrigonometric(1, 1);
    }

    // without calculate
    static public function division(IComplexTrigonometric $complex1, IComplexTrigonometric $complex2)
    {
        return new ComplexTrigonometric(1,1);
    }

}
