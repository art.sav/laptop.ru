<?php

namespace App\Interfaces;

/**
 * Interface Complex Number in Algebraic Form
 */
interface IComplexAlgebraic
{

    public function setReal($real): IComplexAlgebraic;

    public function setImaginary($imaginary): IComplexAlgebraic;

    public function getReal();

    public function getImaginary();

    public function getConjugate(): IComplexAlgebraic;
}
