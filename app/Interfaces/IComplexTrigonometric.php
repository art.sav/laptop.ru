<?php

namespace App\Interfaces;


/**
 * Interface Complex Number in Algebraic Form
 */
interface IComplexTrigonometric
{

    public function setModule($real): IComplexTrigonometric;

    public function setArgument($imaginary): IComplexTrigonometric;

    public function getModule();

    public function getArgument();

}
