<?php

namespace App\Models;

use \App\Interfaces\{IComplexTrigonometric, IComplexBase};

/*
    Complex Number in Trigonometric Form
*/
class ComplexTrigonometric implements IComplexTrigonometric, IComplexBase
{
    /*
     * module
    */
    private $module = 1;

    /*
     * argument
    */
    private $argument = 1;

    public function __construct($module, $argument)
    {
        $this->setModule($module);
        $this->setArgument($argument);
    }

    public function setModule($module): IComplexTrigonometric
    {
        $this->module = $module;
        return $this;
    }

    public function setArgument($argument): IComplexTrigonometric
    {
        $this->argument = $argument;
        return $this;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function getArgument()
    {
        return $this->argument;
    }

}
