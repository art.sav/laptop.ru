<?php

namespace App\Models;

use \App\Interfaces\{IComplexAlgebraic, IComplexBase};

/*
    Complex Number in Algebraic Form
*/
class ComplexAlgebraic implements IComplexAlgebraic, IComplexBase
{
    /*
     * real
    */
    private $real = 1;

    /*
     * imaginary
    */
    private $imaginary = 1;

    public function __construct($real, $imaginary)
    {
        $this->setReal($real);
        $this->setImaginary($imaginary);
    }

    public function setReal($real): IComplexAlgebraic
    {
        $this->real = $real;
        return $this;
    }

    public function setImaginary($imaginary): IComplexAlgebraic
    {
        $this->imaginary = $imaginary;
        return $this;
    }

    public function getReal()
    {
        return $this->real;
    }

    public function getImaginary()
    {
        return $this->imaginary;
    }

    public function getConjugate(): IComplexAlgebraic
    {
        return new self($this->getReal(), 0 - $this->getImaginary());
    }

}
