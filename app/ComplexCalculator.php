<?php

namespace App;

use \App\Exceptions\{InvalidAgrumentsException, InvalidCallMethodException, NeedConvertException};
use \App\Interfaces\{IComplexAlgebraic, IComplexTrigonometric};


/*
    Calculator for Complex Number in Algebraic Form
*/
class ComplexCalculator
{

    public static function __callStatic($name, $arguments)
    {
        if(count($arguments) != 2) {
            throw new InvalidAgrumentsException("", 1);
        }

        if(class_implements($arguments[0]) != class_implements($arguments[1])) {
            throw new NeedConvertException("", 1);
        }

        $calucators = [
            'IComplexAlgebraic' => 'App\\Calculators\\ComplexCalculatorAlgebraic',
            'IComplexTrigonometric' => 'App\\Calculators\\ComplexCalculatorTrigonometric',
        ];


        /* при большем кол-во интерфейсов можно сделать поиск среди указанных у класса */

        if($arguments[0] instanceof IComplexAlgebraic) {
            $calucator = $calucators['IComplexAlgebraic'];
        } else if($arguments[0] instanceof IComplexTrigonometric) {
            $calucator = $calucators['IComplexTrigonometric'];
        } else {
            throw new InvalidAgrumentsException("", 1);
        }

        if(!method_exists($calucator,$name)) {
            throw new InvalidCallMethodException("", 1);
        }

        return forward_static_call ( [$calucator, $name] , ...$arguments );

    }


}
