<?php

use PHPUnit\Framework\TestCase;

use \App\Models\{ComplexAlgebraic, ComplexTrigonometric};
use \App\ComplexCalculator;
use \App\Exceptions\{InvalidAgrumentsException, NeedConvertException, InvalidCallMethodException};

class ComplexCalucatorTest extends TestCase
{
    public function testInvalidAgruments()
    {
        $this->expectException(InvalidAgrumentsException::class);

        $complex1 = new ComplexAlgebraic(1,1);
        $complex2 = new ComplexAlgebraic(1,1);
        $complex3 = new ComplexAlgebraic(1,1);

        $complexResCalc = ComplexCalculator::addition($complex1, $complex2, $complex3);
    }

    public function testNeedConvert()
    {
        $this->expectException(NeedConvertException::class);

        $complex1 = new ComplexTrigonometric(1,1);
        $complex2 = new ComplexAlgebraic(1,1);

        $complexResCalc = ComplexCalculator::addition($complex1, $complex2);
    }

    public function testNeedConvert2()
    {
        $this->expectException(NeedConvertException::class);

        $complex1 = new ComplexTrigonometric(1,1);
        $complex2 = new ComplexTrigonometric(1,1);

        $complexResCalc = ComplexCalculator::addition($complex1, $complex2);
    }

    public function testInvalidMethods()
    {
        $this->expectException(InvalidCallMethodException::class);

        $complex1 = new ComplexTrigonometric(1,1);
        $complex2 = new ComplexTrigonometric(1,1);

        $complexResCalc = ComplexCalculator::invalidMethod($complex1, $complex2);
    }

    public function testAddition()
    {
        $complex1 = new ComplexAlgebraic(1,5);
        $complex2 = new ComplexAlgebraic(2,6);

        $complexRes = new ComplexAlgebraic(3,11);
        $complexResCalc = ComplexCalculator::addition($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,5);
        $complex2 = new ComplexAlgebraic(2,-6);

        $complexRes = new ComplexAlgebraic(1,-1);
        $complexResCalc = ComplexCalculator::addition($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(1,-5);
        $complex2 = new ComplexAlgebraic(-2,6);

        $complexRes = new ComplexAlgebraic(-1,1);
        $complexResCalc = ComplexCalculator::addition($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,-5);
        $complex2 = new ComplexAlgebraic(-2,-6);

        $complexRes = new ComplexAlgebraic(-3,-11);
        $complexResCalc = ComplexCalculator::addition($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);
    }

    public function testSubtraction()
    {
        $complex1 = new ComplexAlgebraic(1,5);
        $complex2 = new ComplexAlgebraic(2,6);

        $complexRes = new ComplexAlgebraic(-1,-1);
        $complexResCalc = ComplexCalculator::subtraction($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,5);
        $complex2 = new ComplexAlgebraic(2,-6);

        $complexRes = new ComplexAlgebraic(-3,11);
        $complexResCalc = ComplexCalculator::subtraction($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(1,-5);
        $complex2 = new ComplexAlgebraic(-2,6);

        $complexRes = new ComplexAlgebraic(3,-11);
        $complexResCalc = ComplexCalculator::subtraction($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,-5);
        $complex2 = new ComplexAlgebraic(-2,-6);

        $complexRes = new ComplexAlgebraic(1,1);
        $complexResCalc = ComplexCalculator::subtraction($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

    }

    public function testMultiplication()
    {
        $complex1 = new ComplexAlgebraic(1,5);
        $complex2 = new ComplexAlgebraic(2,6);

        $complexRes = new ComplexAlgebraic(-28,16);
        $complexResCalc = ComplexCalculator::multiplication($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,5);
        $complex2 = new ComplexAlgebraic(2,-6);

        $complexRes = new ComplexAlgebraic(28,16);
        $complexResCalc = ComplexCalculator::multiplication($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(1,-5);
        $complex2 = new ComplexAlgebraic(-2,6);

        $complexRes = new ComplexAlgebraic(28,16);
        $complexResCalc = ComplexCalculator::multiplication($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,-5);
        $complex2 = new ComplexAlgebraic(-2,-6);

        $complexRes = new ComplexAlgebraic(-28,16);
        $complexResCalc = ComplexCalculator::multiplication($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(1.76,43.65);
        $complex2 = new ComplexAlgebraic(768.2,62.34);

        $complexRes = new ComplexAlgebraic(-1369.109,33641.6484);
        $complexResCalc = ComplexCalculator::multiplication($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);


        $complex1 = new ComplexTrigonometric(1,1);
        $complex2 = new ComplexTrigonometric(1,1);

        $complexRes = new ComplexTrigonometric(1,1);
        $complexResCalc = ComplexCalculator::multiplication($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);
    }

    public function testDivision()
    {
        $complex1 = new ComplexAlgebraic(1,5);
        $complex2 = new ComplexAlgebraic(2,6);

        $complexRes = new ComplexAlgebraic(0.8,0.1);
        $complexResCalc = ComplexCalculator::division($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,5);
        $complex2 = new ComplexAlgebraic(2,-6);

        $complexRes = new ComplexAlgebraic(-0.8,0.1);
        $complexResCalc = ComplexCalculator::division($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(1,-5);
        $complex2 = new ComplexAlgebraic(-2,6);

        $complexRes = new ComplexAlgebraic(-0.8,0.1);
        $complexResCalc = ComplexCalculator::division($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

        $complex1 = new ComplexAlgebraic(-1,-5);
        $complex2 = new ComplexAlgebraic(-2,-6);

        $complexRes = new ComplexAlgebraic(0.8,0.1);
        $complexResCalc = ComplexCalculator::division($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);


        $complex1 = new ComplexAlgebraic(1.76,43.65);
        $complex2 = new ComplexAlgebraic(768.2,62.34);

        $complexRes = new ComplexAlgebraic(0.006856991406870897,0.05626469038752364);
        $complexResCalc = ComplexCalculator::division($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);



        $complex1 = new ComplexTrigonometric(1,1);
        $complex2 = new ComplexTrigonometric(1,1);

        $complexRes = new ComplexTrigonometric(1,1);
        $complexResCalc = ComplexCalculator::division($complex1, $complex2);

        $this->assertEquals($complexRes, $complexResCalc);

    }
}
